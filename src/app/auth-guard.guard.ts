// auth.guard.ts
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    const sessionStart = localStorage.getItem('sessionStart');
    const now = new Date();

    if (sessionStart && now.getTime() - parseInt(sessionStart) > 55* 60 * 1000 ) {
      // La sesión ha expirado
      localStorage.removeItem('sessionStart'); // Limpia el almacenamiento local
      localStorage.removeItem('auth_token');
      this.router.navigate(['agendarUFPS']); // Redirige al home

      return false;
    }

    return true; // La sesión es válida, permite el acceso a la ruta
  }
}

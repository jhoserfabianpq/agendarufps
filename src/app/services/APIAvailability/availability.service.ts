import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Availability } from '../../pages/availability/availability.model';

@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {
  private host = environment.host;
  private availabilityAPI=this.host+environment.AvailabilityBaseEndpoint;

  constructor(
    private http: HttpClient
    ) {}

  getAvailability() {
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.get(this.availabilityAPI, { headers });
  }
  postTopic(availability: any) {
    
    
    const token = localStorage.getItem('auth_token');
    const body = {
      name: availability.name,
      createdBy: availability.createdBy,
      weekDays: availability.weekDays
    };
    
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.post(this.availabilityAPI, body, { headers });
  }
  deleteAvailability(id: number) {
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.delete(this.availabilityAPI + id, { headers });
  }
  putAvailability(availability: Availability) {
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.put(this.availabilityAPI, availability, { headers });
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private host = environment.host;
  private loginGoogle=this.host+environment.loginGoogleBaseEndpoint;

  constructor( private http: HttpClient) { }

  getGoogleLoginURL() {
    
    return this.http.get(this.loginGoogle);
  }

  getToken(code: string) {
    
    return this.http.get(this.host+'/auth/callback?code=' + code);
  }

}

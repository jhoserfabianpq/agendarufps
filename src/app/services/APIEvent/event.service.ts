import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private host = environment.host;
  private urlBaseEvent = environment.eventBaseEndpoint;
  private urlBaseEventPublic = environment.eventBaseEndpointPublic;
  private urlBaseUser = environment.userBaseEndpoint;
  constructor(private http: HttpClient) { }

  getEvents(nickname:string) {
    
    
      const token = localStorage.getItem('auth_token');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.host+this.urlBaseEvent+nickname+'/', {headers});
  }

  getEventByName(name:string,nickname:string){
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.get(this.host+this.urlBaseEvent+nickname+'/'+name+'/', {headers});
  }

  getEventTopicsPublic(nickname:string, eventName:string){
    
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'/topics');
  }
  getEventPublic(nickname:string, eventName:string){
    
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'/');
  }

  getEventPublicIntervals(nickname:string, eventName:string,date:string){
    
    return this.http.get(this.host+this.urlBaseEventPublic+nickname+'/'+eventName+'?date='+date);
  }

  postEvent(event: any,nickname:string) {
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.post(this.host+this.urlBaseEvent+nickname+'/', event, { headers });
  }

  deleteEvent(nickname:string,eventName:string) {
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.delete(this.host+this.urlBaseEvent+nickname+'/'+eventName+'/', { headers });
  }
  updateEvent(nickname:string,event:any) {
    
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.put(this.host+this.urlBaseEvent+nickname+'/'+ event.name +'/',event, { headers });
  }


}

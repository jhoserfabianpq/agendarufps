import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Topic } from '../../pages/topic/topic.model';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  private host = environment.host;
  private topicAPI=this.host+environment.topicBaseEndpoint;

  constructor(
    private http: HttpClient
    ) { }

  getTopics() {
      const token = localStorage.getItem('auth_token');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.topicAPI, { headers });
  }
  getTopicsById(id: string) {
      const token = localStorage.getItem('auth_token');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
      return this.http.get(this.topicAPI+id, { headers });
  }

  postTopic(topic:any) {
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const body = {
      name: topic.topicName
    };
    
    return this.http.post(this.topicAPI, body, { headers });
  }

  deleteTopic(id:any) {
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.http.delete(this.topicAPI+id, { headers });
  }

  updateTopic(id:number,name:string) {
    
    
    
    const token = localStorage.getItem('auth_token');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    const body = {
      id_topic: id,
      name: name
    };
    return this.http.put(this.topicAPI, body, { headers });
  }

}

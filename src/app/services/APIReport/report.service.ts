import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private host = environment.host;
  private reportBaseEnpoint = environment.reportBaseEnpoint;
  private reportAppointment = environment.reportBaseEnpointAppointment;

  constructor(
    private http: HttpClient
  ) { }

getReportGeneral(email: string,period: string) {
  const token = localStorage.getItem('auth_token');
  const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  return this.http.get(this.host + this.reportBaseEnpoint+'?email='+email+'&period='+period, { headers });
}
getReportAppointment(period: string){
  const token = localStorage.getItem('auth_token');
  const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
  return this.http.get(this.host + this.reportAppointment+period, { headers });
}

}

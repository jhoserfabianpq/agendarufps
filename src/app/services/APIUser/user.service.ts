import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private host = environment.host;
  private urlBaseUser = environment.userBaseEndpoint;


  constructor(private http: HttpClient) { }

  getUser(){
    const token = localStorage.getItem('auth_token');
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    return this.http.get(this.host+this.urlBaseUser, headers);
  }




}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../assets/environments/environments';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  private host = environment.host;
  private appointmentAPI=this.host+environment.appointmentBaseEndpoint;
  constructor(
    private http: HttpClient
  ) { }

  postAppointment(appointment:any) {
    const token = localStorage.getItem('auth_token');
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    const body = appointment;
    
    return this.http.post(this.appointmentAPI, body, headers);
  }

  getAllCreatorAppointments(){
    const token = localStorage.getItem('auth_token');
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    return this.http.get(this.appointmentAPI+'my/', headers);
  }

  getUpcomingAppointments(){
    const token = localStorage.getItem('auth_token');
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    return this.http.get(this.appointmentAPI+'upcoming', headers);
  }

  getAppointmentById(id:number){
    const token = localStorage.getItem('auth_token');
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    return this.http.get(this.appointmentAPI+id+'/', headers);
  }

  markAttended(idApointment:number, idUser:number,attended: boolean){
    
    const token = localStorage.getItem('auth_token');
    
    const headers = {headers: {'Authorization': `Bearer ${token}`}};
    
    return this.http.put(this.appointmentAPI+idApointment+'/user/'+idUser+'/attendance?attended='+attended,null, headers);
  }


}

import { Component, ElementRef, ViewChild } from '@angular/core';
import { LayoutService } from '../layout/service/layout.service';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrl: './topbar.component.css',
  providers: [ConfirmationService]
})
export class TopbarComponent {

  user:any;

  items!: MenuItem[];

  @ViewChild('menubutton') menuButton!: ElementRef;

  @ViewChild('topbarmenubutton') topbarMenuButton!: ElementRef;

  @ViewChild('topbarmenu') menu!: ElementRef;

  constructor(
    public layoutService: LayoutService,
    private confirmationService: ConfirmationService,
    private router: Router,
    ) {
    this.user = JSON.parse(localStorage.getItem('user')|| '{}');
    
  }

  confirmDelete(event: Event) {
    this.confirmationService.confirm({
        target: event.target as EventTarget,
        message: '¿Quiere cerrar sesion?',
        icon: 'pi pi-info-circle',
        acceptButtonStyleClass: 'p-button-danger p-button-sm',
        accept: () => {
          localStorage.removeItem('user');
          localStorage.removeItem('auth_token');
          localStorage.removeItem('sessionStart');
          this.router.navigate(['/login']);
        }
    });
  }
}

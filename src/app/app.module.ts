import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//my imports
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LayoutModule } from './shared/layout/layout.module';
import { EventComponent } from './pages/event/event.component';
import { AssistantsComponent } from './pages/assistants/assistants.component';
import { TopicComponent } from './pages/topic/topic.component';
import { AvailabilityComponent } from './pages/availability/availability.component';
import { ReportComponent } from './pages/report/report.component';
import { EventModule } from './pages/event/event.module';
import { TopicModule } from './pages/topic/topic.module';
import { AvailabilityModule } from './pages/availability/availability.module';
import { AssistantsModule } from './pages/assistants/assistants.module';
import { LoginComponent } from './pages/login/login.component';
import { LoginModule } from './pages/login/login.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AppointmentComponent } from './pages/appointment/appointment.component';
import { ThanksComponent } from './pages/thanks/thanks.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    EventComponent,
    AssistantsComponent,
    TopicComponent,
    AvailabilityComponent,
    ReportComponent,
    LoginComponent,
    NotFoundComponent,
    AppointmentComponent,
    ThanksComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    LayoutModule,
    EventModule,
    TopicModule,
    AvailabilityModule,
    AssistantsModule,
    LoginModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Topic } from '../topic/topic.model';
import { TopicService } from '../../services/APITopic/topic.service';
import { EventService } from '../../services/APIEvent/event.service';
import { UserService } from '../../services/APIUser/user.service';
import { filter } from 'rxjs';
import { Appointment } from './appointment.model';
import { LoginService } from '../../services/Login/login.service';
import { AppointmentService } from '../../services/APIAppointment/appointment.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrl: './appointment.component.css',
  providers:[MessageService]
})
export class AppointmentComponent {

  nickname!: string ;
  eventName!:string ;
  topics: Topic[] = [];
  intervarls: any[] = [];
  token = false;
  date: Date[] | undefined;
  minDate: Date = new Date();
  event!:any;
  appointment!:any;
  register!: FormGroup;
  loading=false;
  loadingTopics=false;
  loadingIntervals=false;
  locationOptions: any[] =[];


  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private eventServices: EventService,
    private loginService: LoginService,
    private userService: UserService,
    private appointmentServices: AppointmentService,
    private router: Router,
    private messageService: MessageService,
    ) {
    }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
     this.nickname = params['param1'];
     this.eventName = params['param2'];
      
    },
    (err: any) => {
      console.error('Error:', err);
      this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:err.error});
    });
    this.initForm();
    this.getEventTopicsPublic();
    this.getEventPublic();
  }


  async signInWithGoogle(): Promise<string | undefined> {
    localStorage.removeItem('auth_token');
    const urlResponse = await this.loginService.getGoogleLoginURL().toPromise();

    return new Promise<string | undefined>((resolve, reject) => {
        // Comprobar si url.authURL está definido
        if (!urlResponse || !(urlResponse as any).authURL) {
          reject('La URL de autenticación de Google no está disponible.');
          return;
        }

        const popup = window.open((urlResponse as any).authURL, 'GoogleLogin', 'width=600,height=600');

        const checkPopupUrl = setInterval(() => {
            try {
                const token = localStorage.getItem('auth_token');
                if (!popup || popup.closed) {
                    clearInterval(checkPopupUrl);
                    
                    reject('La ventana emergente fue cerrada sin autenticación.');
                } else if (token) {
                    clearInterval(checkPopupUrl);
                    popup?.close();
                    resolve(token); // Resuelve la promesa con el token
                }
            } catch (error) {
                clearInterval(checkPopupUrl);
                console.error("No se puede acceder a la URL de la ventana emergente.", error);
                reject(error); // Rechaza la promesa si ocurre un error
            }
        }, 200);
    });
}
  getLocation(event:any){
    
    if(event.typeMeeting.toLowerCase()=== 'mixto'){
      this.locationOptions = [
        { label: 'Presencial', value:event.location},
        { label: 'Virtual', value: 'meet' }
      ];
    }
    else if (event.typeMeeting.toLowerCase() === "meet") {
      this.locationOptions = [
        { label: 'Virtual', value: 'meet' }
      ];
      
    } else {
      this.locationOptions = [
        { label: 'Presencial', value:event.location}
      ];
      
    }
  }

  getEventTopicsPublic(): void {
    this.loadingTopics = true;
    this.register.get('topics')?.disable();
    this.eventServices.getEventTopicsPublic(this.nickname,this.eventName).subscribe((topics: any) => {
      
      this.topics = topics as Topic[];
      this.loadingTopics = false;
      this.register.get('topics')?.enable();
    },
    (err: any) => {
      console.error('Error:', err);
      this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:err.error});
      this.loadingTopics = false;
    });
  }

  getEventPublic() {
    this.eventServices.getEventPublic(this.nickname,this.eventName).subscribe((event: any) => {
      
      this.getLocation(event);
      this.event = event;
    },
    (err: any) => {
      console.error('Error:', err);
      this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:err.error});
      this.router.navigate(['/notFound']);
    });
  }

  getEventPublicIntervals(date:Date){
    this.loadingIntervals = true;
    this.register.get('hour')?.disable();
    let formatter = new Intl.DateTimeFormat('es-CO', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
    let parts = formatter.formatToParts(date);
    let year = parts.find(part => part.type === 'year')?.value;
    let month = parts.find(part => part.type === 'month')?.value;
    let day = parts.find(part => part.type === 'day')?.value;
    let formattedDate = `${year}-${month}-${day}`;
    
    this.eventServices.getEventPublicIntervals(this.nickname,this.eventName,formattedDate).subscribe((intervals: any) => {
      this.intervarls = intervals.hours;
      this.intervarls=this.intervarls.map(interval => ({ hour: interval }));
      this.register.get('hour')?.enable();
      
      this.loadingIntervals = false;
    },
    (err: any) => {
      console.error('Error:', err);
      this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:err.error});
      this.loadingIntervals = false;
    });
  }

  getDiasDeshabilitados(): number[] {
    

    let mapeoDias: { [key: string]: number } = {
      'Domingo': 0,
      'Lunes': 1,
      'Martes': 2,
      'Miercoles': 3,
      'Jueves': 4,
      'Viernes': 5,
      'Sabado': 6
    };
    let diasHabilitados = this.event.availability.weekDays
    .filter((dia: { state: number; }) => dia.state === 1)
    .map((dia: { name: string | number; }) => mapeoDias[dia.name]);
    let todosLosDias = [0, 1, 2, 3, 4, 5, 6]; // Domingo a sábado
    
    return todosLosDias.filter(dia => !diasHabilitados.includes(dia));
  }

  initForm() {
    this.register = this.formBuilder.group({
      topic: [[],Validators.required],
      comment:['',Validators.required],
      date:[null,Validators.required],
      hour:[null,Validators.required,],
      guests: [[]],
      location:[[],Validators.required],
    });
    this.register.get('hour')?.disable();
  }



  registerForm() {
    this.loading=true;
    let appointment :Appointment ;
    let date=this.register.get('date')?.value;
    let formatter = new Intl.DateTimeFormat('es-CO', {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    });
    let parts = formatter.formatToParts(date);
    let year = parts.find(part => part.type === 'year')?.value;
    let month = parts.find(part => part.type === 'month')?.value;
    let day = parts.find(part => part.type === 'day')?.value;
    let formattedDate = `${year}-${month}-${day}`;
    
    this.signInWithGoogle().then(token => {
      
      if (token) {
        localStorage.setItem('auth_token', token);
      }
      this.userService.getUser().subscribe((user: any) => {
        
        appointment = {
          appointment_start: formattedDate+ 'T' + this.register.get('hour')?.value.hour,
          event_id: {
            id_event: this.event.id_event
          },
          user_appointment: [
            {
              email: user.email
            }
          ],
          topic: {
            id_topic: this.register.get('topic')?.value.id_topic
          },
          comments: this.register.get('comment')?.value,
          location: this.register.get('location')?.value
        };
        
        if(this.register.get('guests')?.value.length>0){
          appointment.user_appointment=(this.register.get('guests')?.value.map((email: string) => ({ email: email })));
          appointment.user_appointment.push({email: user.email});
        }
        this.appointmentServices.postAppointment(appointment).subscribe((appointment: any) => {
          
          this.loading=false;
          this.router.navigate(['/thanks']);
        },
        error => {
          console.error('Error:', error.error);
          this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:error.error});
          this.loading=false;
        }
        );
      },
      (err: any) => {
        console.error('Error:', err);
        this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:err.error});
        this.loading=false;
      });
        // Procesa el token como sea necesario
    }).catch(error => {
        console.error('Error:', error);
        this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:error});
        this.loading=false;

        // Maneja el error adecuadamente
    });


  }

  setEventNamePreview(){
    this.eventName = this.register.get('nameEvent')?.value;
    
  }

}

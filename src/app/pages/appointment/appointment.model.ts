export interface Appointment {
  appointment_start: string,
  event_id: {
    id_event: 6
  },
  user_appointment:Email [],
  topic: {
    id_topic: 3
  },
  comments: string,
  location: string
}

interface Email {
  email: string;
}



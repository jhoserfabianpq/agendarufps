import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TopicService } from '../../services/APITopic/topic.service';
import { Topic } from './topic.model';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrl: './topic.component.css',
  providers: [MessageService]
})
export class TopicComponent {

  formRegister: any;
  display: boolean = false;
  topics!: Topic[];
  topic!: Topic ;
  loandingCreateTopic = false;

  register: FormGroup = new FormGroup({
    topicName: new FormControl('', [Validators.required]),
  });

  constructor(
    private topicAPI: TopicService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.getTopics();
  }



  getTopics() {
    this.topicAPI.getTopics().subscribe(
      (data: any) => {
        
        this.setTopics(data);
      },
      (err: any) => {
        
        this.messageService.add({ severity: 'error', summary: 'Error', detail:err.error });
      })
      ;
  }
  getTopicsById(id: string) {
    this.topicAPI.getTopicsById(id).subscribe(
      (data: any) => {
        this.setTopic(data);
      },
      (err: any) => {
        
        this.messageService.add({ severity: 'error', summary: 'Error', detail:err.error });
      });
  }

  setTopic(data: any) {
    this.topic = data;
  }
  setTopics(data: any) {
    this.topics = data;
  }

  registerTopic() {
    this.loandingCreateTopic = true;
    
    if (this.ValidateForm()) {
      this.formRegister = {
        topicName: this.register.get('topicName')?.value
      }
      this.topicAPI.postTopic(this.formRegister)
        .subscribe((data) => {
          
          this.createdTopic();
          this.resetForm();
          this.addTopic(data as Topic); // Cast the data object to the Topic type
          this.display = false;
          this.loandingCreateTopic = false;
        },
          (err: any) => {
            this.showError();
            
            this.resetForm();
            this.display = false;
            this.messageService.add({ severity: 'error', summary: 'Error', detail:err.error });
          }
        )
    }
  }


  onDeleteTopic(id: number): void {
    this.topics = this.topics.filter(topic => topic.id_topic !== id);
    this.deletedTopic();

  }
  onUpdateTopic(topic:any): void {
    
    const foundTopic = this.topics.find(x => x.id_topic === topic.id);
    if (foundTopic) {
      foundTopic.name = topic.name;
      this.updatedTopic();
    }
    else {
      this.showError();
    }
  }

  addTopic(topic: Topic) {
    this.topics.push(topic);
  }

  showError() {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ocurrio un error.' });
  }
  resetForm() {
    this.register.reset();
  }
  createdTopic() {
    this.messageService.add({ key:'ct', severity: 'success', summary: 'Tema Registrado', detail: 'El tema se registro en la base de datos' });
  }
  deletedTopic() {
    this.messageService.add({ key:'dt', severity: 'warn', summary: 'Tema eliminado', detail: 'El tema se ha eliminado' });
  }
  updatedTopic() {
    this.messageService.add({ key:'ut', severity: 'info', summary: 'Tema Actualizado', detail: 'El tema se ha actualizado' });
  }

  ValidateForm(): boolean {
    this.register.markAllAsTouched();
    return this.register.valid
  }

}

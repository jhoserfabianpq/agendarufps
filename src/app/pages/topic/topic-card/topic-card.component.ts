import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Topic } from '../topic.model';
import { TopicService } from '../../../services/APITopic/topic.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-topic-card',
  templateUrl: './topic-card.component.html',
  styleUrl: './topic-card.component.css',
  providers: [ConfirmationService,MessageService]
})
export class TopicCardComponent {
  displayEdit: boolean = false;
  loandingDeleteTopic= false;
  loandingUpdateTopic= false;
  formRegister:any;
  register: FormGroup = new FormGroup({
    topicName: new FormControl('', [Validators.required]),
  });

  constructor(
    private topicAPI: TopicService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService

  ) {}

  @Input()topic!: Topic;
  @Output() topicDeleted: EventEmitter<number> = new EventEmitter<number>();
  @Output() topicUpdated: EventEmitter<any> = new EventEmitter<any>();

  confirmDelete(event: Event,id:number) {
    this.confirmationService.confirm({
        target: event.target as EventTarget,
        message: '¿Quiere eliminar tema?',
        icon: 'pi pi-info-circle',
        acceptButtonStyleClass: 'p-button-danger p-button-sm',
        accept: () => {
          this.deleteTopic(id);
        }
    });
  }

  deleteTopic(id:number){
    this.loandingDeleteTopic = true;
    
    this.topicAPI.deleteTopic(id).subscribe(
      (data: any) => {
        
        this.topicDeleted.emit(id);
        this.loandingDeleteTopic = false;
      },
      (err: any) => {
        
        this.loandingDeleteTopic = false;
        this.messageService.add({ key:'ue',severity: 'error', summary: 'Error', detail:err.error });
      });
  }

  viewEdit(topic:Topic){
    
    this.displayEdit = true;
    this.register.get('topicName')?.setValue(topic.name);
  }


  updateTopic(id:number){
    this.displayEdit = true;
    this.loandingUpdateTopic= true;
    
    let name=this.register.get('topicName')?.value;
    this.topicAPI.updateTopic(id, name).subscribe(
      (data: any) => {
        
        this.displayEdit = false;
        this.topicUpdated.emit({id, name}); // Fix: Pass the arguments as an object
        this.resetForm();
        this.loandingUpdateTopic= false;
      },
      (err: any) => {
        
        this.loandingUpdateTopic= false;
        this.messageService.add({ key:'ue',severity: 'error', summary: 'Error', detail:err.error });
      });
  }

  resetForm() {
    this.register.reset();
  }

}

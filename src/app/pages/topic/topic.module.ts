import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { TopicCardComponent } from './topic-card/topic-card.component';



@NgModule({
  declarations: [
    TopicCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TopicCardComponent
  ]
})
export class TopicModule { }

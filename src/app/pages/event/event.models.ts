export interface Minute {
  value: number;
  label:string;
}


export interface Availability {
  id: string;
  name: string;
  days: Day[];
}

interface Day {
  name: string;
}
interface Location {
  value: string,
  location:string
}
export interface EventUserCreate {
  typeMeeting: string,
  description: string,
  name: string,
  duration: number,
  location: string,
  state: number,
  availability: {
    id_availability: number
  },
  user_id: {
    email: string
  },
  topics: Topic[]
}


export interface EventUser {
  id_event: number,
  typeMeeting: string,
  description: string,
  name: string,
  duration: number,
  location: string,
  state: number,
  availability: {
    id_availability: number
  },
  user_id: {
    email: string
  },
  topics: Topic[]
}

interface Topic {
  id_topic: number,
  name: string
}

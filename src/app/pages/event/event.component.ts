import { Component } from '@angular/core';
import { EventCard } from './event-card/event-card.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Availability, EventUser, EventUserCreate, Minute} from './event.models';
import { EventService } from '../../services/APIEvent/event.service';
import { UserService } from '../../services/APIUser/user.service';
import { TopicService } from '../../services/APITopic/topic.service';
import { Topic } from '../topic/topic.model';
import { AvailabilityService } from '../../services/APIAvailability/availability.service';
import { MessageService } from 'primeng/api';
import { environment } from '../../../assets/environments/environments';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  /*styleUrl: './event.component.css'*/
  providers: [MessageService]
})


export class EventComponent {

  //Varibales
  display: boolean = false;
  date: Date[] | undefined;
  formRegister!: any;
  value!: number;
  eventName: string | undefined ='Nombre del evento';
  eventDuration:Minute = { value: 0, label:'Min'};
  eventsUser: EventUser[] = [];
  cards!: EventCard[];
  register!: FormGroup;
  topics: Topic[] = [];
  availabilities: Availability[] = [];
  loandingAvailability = false;
  loandingTopic = false;
  loandingSaveEvent = false;

  constructor(
    private eventAPI: EventService,
    private userApi:UserService,
    private topicAPI: TopicService,
    private availabilityAPI: AvailabilityService,
    private formBuilder: FormBuilder,
    private messageService: MessageService
  ) {}


  ngOnInit(): void {
    this.getEvents();
    this.initForm();
  }

  initForm() {
    this.register = this.formBuilder.group({
      nameEvent: ['', Validators.required],
      durationEvent: ['', Validators.required],
      availabilityEvent: ['', Validators.required],
      topicEvent: [[], Validators.required],
      observationEvent: ['',Validators.required],
      locationEvent: [[],Validators.required],
      otherLocation: [''],
      typeMeeting: [''],
      activeEvent: [[],Validators.required],
    });
  }

  async copyLink(eventName:any) {
    try {
      let nickname = JSON.parse(localStorage.getItem('user')|| '{}')?.nickname;
      let linkAppointment=environment.hostFront+environment.appointmentBaseEndpoint;
      await navigator.clipboard.writeText(linkAppointment+nickname+'/'+eventName);
      // Muestra una notificación o mensaje de éxito si lo deseas
      
      this.messageService.add({ key:'cp', severity: 'success', summary: 'Enlace copiado', detail: 'El enlace se ha copiado al portapapeles' });
    } catch (error) {
      // Maneja el caso de error
      console.error('Error al copiar el enlace:', error);
    }
  }


  showCreateEvent(){
    this.display = true;
    this.getTopics();
    this.getAvailabilities();
  }


  onUpdateEvent(card:any){
    
    this.ngOnInit();
  }

  onDeleteEvent(card:any){
    this.cards = this.cards.filter((c: EventCard) => c !== card);
    this.deletedEvent();
  }
  deletedEvent() {
    this.messageService.add({ key:'de', severity: 'warn', summary: 'Evento eliminado', detail: 'El evento se ha eliminado' });
  }

  async registerForm() {
    this.loandingSaveEvent = true;
    if (this.ValidateForm()) {
      this.formRegister = {
        idEvent: this.register.get('idEvent')?.value,
        nameEvent: this.register.get('nameEvent')?.value,
        durationEvent: this.register.get('durationEvent')?.value,
        locationEvent: this.register.get('locationEvent')?.value,
        observationEvent: this.register.get('observationEvent')?.value,
        topicEvent: this.register.get('topicEvent')?.value,
        availabilityEvent: this.register.get('availabilityEvent')?.value,
        otherLocation: this.register.get('otherLocation')?.value,
        activeEvent: this.register.get('activeEvent')?.value,
      }
      let event: EventUserCreate;
      let location =''
      let typeMeeting=''
      let email =this.getUserEmail();
      let nickname = JSON.parse(localStorage.getItem('user') || '{}').nickname;
      

      if(this.formRegister.locationEvent.includes('in person') && this.formRegister.locationEvent.includes('meet')){
        typeMeeting='Mixto'
        location = this.formRegister.otherLocation;
      }
      else if(this.formRegister.locationEvent.includes('meet')) {
        location='meet'
        typeMeeting='meet'
      }
      else if(this.formRegister.locationEvent.includes('in person')) {
        location = this.formRegister.otherLocation;
        typeMeeting=this.formRegister.otherLocation
      }
      event = {
        description: this.formRegister.observationEvent,
        name: this.formRegister.nameEvent,
        duration: this.formRegister.durationEvent.value,
        location: location,
        state: this.formRegister.activeEvent,
        availability: {
          id_availability: this.formRegister.availabilityEvent.id_availability
        },
        user_id: {
          email: await email,
        },
        topics: this.formRegister.topicEvent,
        typeMeeting: typeMeeting,
      }
      this.eventAPI.postEvent(event, await nickname)
        .subscribe((data) => {
          
          this.cards.push({
            title: event.name,
            location: event.location.toString(),
            description: event.description,
            url: "",
            time: event.duration.toString()
          });
          this.createdEvent();
          this.display = false;
          this.loandingSaveEvent = false;
        },
          (err: any) => {
            this.showError();
            
            this.display = false;
          }
        )
    }
  }


  disableForm() {
    this.register.get('idEvent')?.disable();
  }

  setEventNamePreview(){
    this.eventName = this.register.get('nameEvent')?.value;
    
  }

  setEventDurationPreview(){
    this.eventDuration = this.register.get('durationEvent')?.value;
    
  }

  ValidateForm(): boolean {
    this.register.markAllAsTouched();
    return this.register.valid
  }

  getAvailabilities(){
    this.loandingAvailability = true;
    this.availabilityAPI.getAvailability().subscribe(
      (data: any) => {
        this.availabilities = data;
        this.loandingAvailability = false;
      },
      (error: any) => {
        
        this.messageService.add({ key:'cp', severity: 'error', summary: 'Error', detail: error.error });
      }
      );
  }

  async getEvents() {
        let nickname = JSON.parse(localStorage.getItem('user') || '{}').nickname;
        
        this.eventAPI.getEvents(await nickname).subscribe(
          (data: any) => {
            this.setEvents(data);
          },
          (error: any) => {
            
            this.messageService.add({ key:'cp', severity: 'error', summary: 'Error', detail: error.error });
          });
  }

  getTopics(){
    this.loandingTopic = true;
    this.topicAPI.getTopics().subscribe(
      (data: any) => {
        this.topics=data;
        this.loandingTopic = false;
      },
      (error: any) => {
        
        this.messageService.add({ key:'cp', severity: 'error', summary: 'Error', detail: error.error });
      }
      );
  }

  getUserEmail(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.userApi.getUser().subscribe(
        (data: any) => {
          const email = data.email;
          resolve(email);
        },
        (error: any) => {
          reject(error);
        }
      );
    });
  }

  setEvents(data: any) {
    this.eventsUser = data;
    this.cards = this.eventsUser.map((event: EventUser) => {
      return {
        title: event.name,
        location: event.location.toString(),
        description: event.description,
        url: "",
        time: event.duration.toString() // Convert the number to a string
      }
    });
    
  }

  lugarOptions: any[] = [
    { label: 'Virtual', value: 'meet' },
    { label: 'Presencial', value: 'in person' },
  ];

  activeEventOptions: any[] = [
    { label: 'Activo', value: '1' },
    { label: 'Inactivo', value: '0' },
  ];

  minutes: Minute[] = [
    { value: 10, label:'10 Min'},
    { value: 15, label:'15 Min'},
    { value: 20, label:'20 Min'},
    { value: 25, label:'25 Min'},
    { value: 30, label:'30 Min'},
    { value: 35, label:'35 Min'},
    { value: 40, label:'40 Min'},
    { value: 45, label:'45 Min'},
    { value: 50, label:'50 Min'},
    { value: 55, label:'55 Min'},
    { value: 60, label:'60 Min'},
  ];

  createdEvent() {
    this.messageService.add({ key:'ce', severity: 'success', summary: 'Evento Registrado', detail: 'El evento se registro en la base de datos' });
  }

  showError() {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ocurrio un error.' });
  }

  resetForm() {
    this.register.reset();
  }

}

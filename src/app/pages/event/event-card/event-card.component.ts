import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EventCard } from './event-card.model';
import { EventService } from '../../../services/APIEvent/event.service';
import { UserService } from '../../../services/APIUser/user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Availability, EventUser, Minute } from '../event.models';
import { Topic } from '../../topic/topic.model';
import { ConfirmationService, MessageService } from 'primeng/api';
import { TopicService } from '../../../services/APITopic/topic.service';
import { AvailabilityService } from '../../../services/APIAvailability/availability.service';
import { setThrowInvalidWriteToSignalError } from '@angular/core/primitives/signals';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrl: './event-card.component.css',
  providers: [ConfirmationService,MessageService]
})
export class EventCardComponent {

  displayEdit: boolean = false;
  formRegister:any;
  register: any;
  date: Date[] | undefined;
  eventName: string | undefined ='Nombre del evento';
  eventDuration:Minute = { value: 0, label:'Min'};
  topics: Topic[] = [];
  availabilities: Availability[] = [];
  delected=false;
  loandingAvailability=false;
  loandingTopic=false;
  loandingObservation=false;
  loandingSaveEvent=false;


  constructor(
  private eventService: EventService,
  private userService: UserService,
  private formBuilder: FormBuilder,
  private userApi:UserService,
  private eventAPI: EventService,
  private topicAPI: TopicService,
  private availabilityAPI: AvailabilityService,
  private messageService: MessageService,
  private confirmationService: ConfirmationService


  ) {
    this.initForm();
  }

  @Input()card!: EventCard;
  @Output() eventDeleted: EventEmitter<any> = new EventEmitter<any>();
  @Output() eventUpdated: EventEmitter<any> = new EventEmitter<any>();
  @Output() copiLink: EventEmitter<any> = new EventEmitter<any>();

  copyLink(linkToCopy:string) {
      this.copiLink.emit(linkToCopy);
  }

  initForm() {
    this.register = this.formBuilder.group({
      nameEvent: ['', Validators.required],
      durationEvent: ['', Validators.required],
      availabilityEvent: ['', Validators.required],
      topicEvent: [[], Validators.required],
      observationEvent: ['',Validators.required],
      locationEvent: [[],Validators.required],
      otherLocation: [''],
      id_event: [''],
      typeMeeting: [''],
      activeEvent: [[],Validators.required],
      state: ['']
    });
  }

  confirmDelete(event: Event,card:EventCard) {
    this.confirmationService.confirm({
        target: event.target as EventTarget,
        message: '¿Quiere eliminar el evento?, esto eliminara los registros de usuarios agendaros',
        icon: 'pi pi-info-circle',
        acceptButtonStyleClass: 'p-button-danger p-button-sm',
        accept: () => {
          this.deleteEvent(card);
        }
    });
}

  deleteEvent(card:EventCard){
    this.delected=true;
    
    
    this.userService.getUser().subscribe(
      (user: any) => {
        
        this.eventService.deleteEvent(user.nickname, card.title).subscribe(
          (data: any) => {
            
            this.eventDeleted.emit(card);
            this.delected=false;
          },
          (error: any) => {
            
            this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error });
            this.delected=false;
          })
          ;
      }
    ,
    (error: any) => {
      
      this.delected=false;
      this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error });
    });
  }

  lugarOptions: any[] = [
    { label: 'Virtual', value: 'meet' },
    { label: 'Presencial', value: 'in person' },
  ];

  activeEventOptions: any[] = [
    { label: 'Activo', value: '1' },
    { label: 'Inactivo', value: '0' },
  ];

  minutes: Minute[] = [
    { value: 10, label:'10 Min'},
    { value: 15, label:'15 Min'},
    { value: 20, label:'20 Min'},
    { value: 25, label:'25 Min'},
    { value: 30, label:'30 Min'},
    { value: 35, label:'35 Min'},
    { value: 40, label:'40 Min'},
    { value: 45, label:'45 Min'},
    { value: 50, label:'50 Min'},
    { value: 55, label:'55 Min'},
    { value: 60, label:'60 Min'},
  ];

  setEventDurationPreview(){
    this.eventDuration = this.register.get('durationEvent')?.value;
    
  }
  setEventNamePreview(){
    this.eventName = this.register.get('nameEvent')?.value;
    
  }

  ValidateForm(): boolean {
    this.register.markAllAsTouched();
    return this.register.valid
  }

  getEventByName(name:string,nickname:string){
    return new Promise<any> ((resolve, reject) => {
      this.eventAPI.getEventByName(name,nickname).subscribe(
        (data: any) => {
          resolve(data);
        },
        (error: any) => {
          reject(error);
        }
      );
    });
  }

  async showUpdateEvent(event:any){
    this.displayEdit = true;
    this.getTopics();
    this.getAvailabilities();
    let name=event.title;
    try {
      this.loandingObservation=true;
      let eventToEdit: any = await this.getEventToEdit(name);
      
      if (eventToEdit) {
        this.register.get('nameEvent')?.setValue(eventToEdit.name);
        this.register.get('durationEvent')?.setValue({label:eventToEdit.duration+" Min", value:eventToEdit.duration});
        this.register.get('topicEvent')?.setValue(eventToEdit.topics);
        this.register.get('availabilityEvent')?.setValue(eventToEdit.availability);
        this.register.get('observationEvent')?.setValue(eventToEdit.description);
        this.loandingObservation=false;
        let location = [{}]
        let otherLocation='';
        if(eventToEdit.typeMeeting==='Mixto'){
          location.unshift('meet');
          location.unshift('in person');
          otherLocation = eventToEdit.location;
        }
        else if(eventToEdit.typeMeeting==='meet'){
          location.unshift('meet');
        }
        else if(eventToEdit.typeMeeting){
          location.unshift('in person');
          otherLocation = eventToEdit.location;
        }
        this.register.get('otherLocation')?.setValue(otherLocation);
        this.register.get('locationEvent')?.setValue(location);
        this.register.get('id_event')?.setValue(eventToEdit.id_event);
        this.register.get('typeMeeting')?.setValue(eventToEdit.typeMeeting);
        
        this.register.get('activeEvent')?.setValue(String(eventToEdit.state));
      } else {
        // Manejar el caso de no encontrar el evento o datos inválidos
        console.error('Evento para editar no encontrado o inválido');
        this.displayEdit=false;
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ocurrio un error.' });
      }
    } catch (error) {
      // Manejar errores que puedan ocurrir durante getEventToEdit
      console.error('Error al obtener el evento para editar', error);
      this.displayEdit=false;
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ocurrio un error.' });
    }
  }

  async getEventToEdit(eventName: string): Promise<any> {
    
    // Esperar y obtener el nickname de forma asíncrona
    const nickname = JSON.parse(localStorage.getItem('user')|| '')?.nickname;
    // Convertir la llamada del servicio a una promesa
    return new Promise((resolve, reject) => {
      this.eventService.getEventByName(eventName, nickname).subscribe({
        next: (data: any) => {
          resolve(data); // Resuelve la promesa con los datos recibidos
        },
        error: (error) => {
          console.error(error);
          reject(error); // Rechaza la promesa si hay un error
        }
      });
    });
  }

  async updateEvent(newEvent:any) {
    this.loandingSaveEvent=true;
    let nickname: string;
    nickname = JSON.parse(localStorage.getItem('user')|| '')?.nickname;
    let event: EventUser;
    let location =''
    let typeMeeting=''
    let email = JSON.parse(localStorage.getItem('user')|| '')?.email;
    event = await this.getEventByName(newEvent.title,nickname);
      this.formRegister = {
        idEvent: this.register.get('idEvent')?.value,
        nameEvent: this.register.get('nameEvent')?.value,
        durationEvent: this.register.get('durationEvent')?.value,
        locationEvent: this.register.get('locationEvent')?.value,
        observationEvent: this.register.get('observationEvent')?.value,
        topicEvent: this.register.get('topicEvent')?.value,
        availabilityEvent: this.register.get('availabilityEvent')?.value,
        otherLocation: this.register.get('otherLocation')?.value,
        id_event: this.register.get('id_event')?.value,
        typeMeeting: this.register.get('typeMeeting')?.value,
        state: this.register.get('activeEvent')?.value
      }
      if(this.formRegister.locationEvent.includes('in person') && this.formRegister.locationEvent.includes('meet')){
        typeMeeting='Mixto'
        location = this.formRegister.otherLocation;
      }
      else if(this.formRegister.locationEvent.includes('meet')) {
        location='meet'
        typeMeeting='meet'
      }
      else if(this.formRegister.locationEvent.includes('in person')) {
        location = this.formRegister.otherLocation;
        typeMeeting=this.formRegister.otherLocation
      }
      event = {
        description: this.formRegister.observationEvent,
        name: this.formRegister.nameEvent,
        duration: this.formRegister.durationEvent.value,
        location: location,
        state: Number(this.formRegister.state),
        availability: {
          id_availability: this.formRegister.availabilityEvent.id_availability
        },
        user_id: {
          email: email,
        },
        topics: this.formRegister.topicEvent,
        id_event: this.formRegister.id_event,
        typeMeeting: typeMeeting
      }
      this.eventAPI.updateEvent(nickname, event)
        .subscribe((data) => {
          this.eventUpdated.emit(event);
          this.displayEdit = false;
          this.updatedEvent();
          this.loandingSaveEvent=false;
        },
          (err: any) => {
            this.showError();
            
            this.displayEdit = false;
            this.loandingSaveEvent=false;
            this.messageService.add({ severity: 'error', summary: 'Error', detail: err.error });
          }
        )

  }

  getTopics(){
    this.loandingTopic=true;
    this.topicAPI.getTopics().subscribe(
      (data: any) => {
        this.topics=data;
        this.loandingTopic=false;
      },
      (error: any) => {
        
        this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error });
      });
  }

  getAvailabilities(){
    this.loandingAvailability=true;
    this.availabilityAPI.getAvailability().subscribe(
      (data: any) => {
        this.availabilities = data;
        this.loandingAvailability=false;
      },
      (error: any) => {
        
        this.messageService.add({ key:'cp', severity: 'error', summary: 'Error', detail: error.error });
      });
  }

  updatedEvent() {
    this.messageService.add({ key:'ue', severity: 'success', summary: 'Evento Actualizado', detail: 'El evento se actualizo en la base de datos' });
  }
  showError() {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Ocurrio un error.' });
  }

}

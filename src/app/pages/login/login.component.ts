import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../shared/layout/service/layout.service';
import { environment } from '../../../assets/environments/environments';
import { LoginService } from '../../services/Login/login.service';
import { GooogleLoginURL } from './login.model';
import { TokenService } from '../../services/Login/token.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserService } from '../../services/APIUser/user.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: [MessageService]
})
export class LoginComponent {
  host=environment.host;
  loginGoogle!:string ;
  isLogin = false;
  googleLogin=false;

  constructor(
    public layoutService: LayoutService,
    private loginService: LoginService,
    private token: TokenService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private userService:UserService
    ) {}

    ngOnInit(): void {
    this.getGoogleLoginURL();
    this.getToken();
    this.getUser();
  }

  async getUser() {
    const data = await firstValueFrom(this.userService.getUser());
    
    localStorage.setItem('user', JSON.stringify(data));
  }


  getGoogleLoginURL() {
    this.loginService.getGoogleLoginURL().subscribe(
      (data: any) => {
      this.setGoogleLoginURL(data);
    },
    (error:any) => {
      console.error('Error obteniendo el URL de inicio de sesión con Google:', error);
      this.messageService.add(
        {severity:'error', summary:'Error', detail:'No se ha podido iniciar sesión con Google. Inténtelo de nuevo más tarde.'}
      );
    });
  }
  setGoogleLoginURL(url:GooogleLoginURL) {
    
    this.loginGoogle =url.authURL;
  }

  loginWithGoogle() {
    this.googleLogin=true;
    this.token.removeToken();
    window.location.href = this.loginGoogle;
  }


  getToken() {
    this.route.queryParams.subscribe(params => {
      if (params['code']) {
        this.googleLogin=true;
        this.loginService.getToken(params['code']).subscribe(
          (data: any) => {
            this.token.setToken(data.token);
            this.setLogin(true);
          },
          error => {
            console.error('Error obteniendo el token:', error);
            this.googleLogin=false;
            this.router.navigate(['/login']);
            this.messageService.add(
              {severity:'error', summary:'Error', detail:'No se ha podido iniciar sesión con Google. Inténtelo de nuevo más tarde.'}
            );
          }
        );
      }
      this.isLogin=false;
    });
  }

  async checkLoginAndRedirect() {
    
    if (this.isLogin) {
      await this.getUser();
      const now = new Date();
      localStorage.setItem('sessionStart', now.getTime().toString());
      // También en tu servicio de autenticación, después de guardar el momento de inicio de sesión
      setTimeout(() => {
        // Redirige al usuario al home o muestra un mensaje indicando que la sesión ha expirado
        this.router.navigate(['/login']); // Asegúrate de inyectar Router de @angular/router en tu servicio
      }, 50 * 60 * 1000); // 50 minutos en milisegundos
      this.router.navigate(['/app/eventos']);
    } else {
      this.router.navigate(['/']);
    }
  }

  setLogin(login: boolean) {
    this.isLogin = login;
    
    this.checkLoginAndRedirect();
  }

}

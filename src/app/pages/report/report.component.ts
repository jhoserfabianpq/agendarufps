import { Component, ElementRef, ViewChild, effect } from '@angular/core';
import { ReportService } from '../../services/APIReport/report.service';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrl: './report.component.css',
  providers: [MessageService]
})
export class ReportComponent {

  rangeDates: Date[] = [];
  chartsData: {[key: string]: any} = {};
  totalscheduled:Array<number>= [];
  totalesAppointment:Array<number>= [];
  totalVirutalAppointments:Array<number>= [];
  totalInPersonAppointments:Array<number>= [];
  averageTime:Array<number>= [];
  data: any;
  options: any ;
  textColor:any;
  textColorSecondary:any;
  surfaceBorder:any;
  loanding=false;
  loandingTable=false;
  events:Array<any>= [];
  selectedEvent: any;
  years!:Array<any>;
  selectedYear:any;
  period:any=[
    {name:'Primer Semestre', value:'01'},
    {name:'Segundo Semestre', value:'02'}
  ];
  selectedPeriod:any;
  user =JSON.parse(localStorage.getItem('user')|| '{}');
  appointmentData:any;
  myEmail!: string;
  loadingTable!: any[] ;
  loadingReport=false;

  @ViewChild('filter') filter!: ElementRef;
  @ViewChild('dt1') table: Table | undefined;

  constructor(
    private reportService: ReportService,
    private messageService: MessageService
  ){}

  ngOnInit(): void {
    this.initChart();
    this.getyears();
    this.loadingTable = Array.from({ length: 5 }).map((_, i) => `Item #${i}`);
  }

  exportarComoImagen() {
    const cardElement = document.querySelector('.imagenReport') as HTMLElement;
    if (cardElement) {
      html2canvas(cardElement).then(canvas => {
        // Crear un elemento <a> para permitir la descarga de la imagen
        const image = canvas.toDataURL('image/png', 1.0);
        const link = document.createElement('a');
        link.href = image;
        link.download = 'informe.png';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
    }
  }

  exportCSVCustom(): void {
    this.loadingReport = true;
    const headers = 'Nombre Evento,Ver Evento,Tipo Evento,Lugar Evento,Tema Agendado,Fecha Evento,Hora Evento,Duracion Evento,Asistentes\n';
    const rows = this.appointmentData.map((event: { attendees: { attended: any; user: { first_name: any; last_name: any; email: any; }; }[]; appointment: { event_id: { name: any; typeMeeting: any; location: any; duration: any; }; link_google_calendar: any; topic: { name: any; }; appointment_start: string | number | Date; }; }) => {
      const assistantList = event.attendees
        .map((assistant: { attended: any; user: { first_name: any; last_name: any; email: any; }; }) => {
          // Aquí decides qué información del asistente incluir
          const attendedStatus = assistant.attended ? 'Asistió' : 'No Asistió';
          return assistant.user.first_name ? `${assistant.user.first_name} ${assistant.user.last_name} (${assistant.user.email}) - ${attendedStatus}` : `Usuario No Registrado - ${attendedStatus}`;
        })
        .join('; '); // Separa cada asistente con '; '
      return [
        `"${event.appointment.event_id.name}"`,
        `"${event.appointment.link_google_calendar}"`,
        `"${event.appointment.event_id.typeMeeting}"`,
        `"${event.appointment.event_id.location}"`,
        `"${event.appointment.topic.name}"`,
        `"${new Date(event.appointment.appointment_start).toLocaleDateString()}"`,
        `"${new Date(event.appointment.appointment_start).toLocaleTimeString()}"`,
        `"${event.appointment.event_id.duration} Min"`,
        `"${assistantList}"` // Añade la lista de asistentes con el estado de asistencia
      ].join(',');
    });

    const csvContent = headers + rows.join('\n');
    this.loadingReport = false;
    this.exportarComoImagen();
    this.downloadCSV(csvContent);
    }

    downloadCSV(csvContent: string): void {
      const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', 'events.csv');
      document.body.appendChild(link); // Necesario para Firefox
      link.click();
      document.body.removeChild(link);
      }

  onGlobalFilter(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.table?.filterGlobal(value, 'contains');
  }

  clear(table: Table) {
    table.clear();
  }

  getReportAppointment(){
    this.appointmentData=null;
    this.loandingTable=true;
    this.reportService.getReportAppointment(this.selectedYear.value+this.selectedPeriod.value).subscribe(
      (data: any) => {
        console.log('year: '+this.selectedYear.value,'semestre: '+this.selectedPeriod.value);
        console.log('data: ',data);
        this.appointmentData=data;
        this.loandingTable=false;
      },
      (error: any) => {
        console.log(error);
        this.messageService.add({severity:'error', summary:'Error', detail: error.error});
        this.loanding=false;
      }
    );
  }

  getReportGeneral() {
    this.events=[];
    this.chartsData={};
    this.data={};
    let email=this.user.email;
    let period=this.selectedYear.value+this.selectedPeriod.value;
    this.loanding=true;
    this.reportService.getReportGeneral(email,period).subscribe(
      (data: any) => {
        const groupedData = data.reduce((acc: any, item:any) => {
          // Si no existe el eventName, lo inicializa

          if (!acc[item.eventName]) {
            acc[item.eventName] = {};
            console.log('item: ',item.eventName);
            this.events.push({name:item.eventName,value:item.eventName})
          }
          // Si no existe el topicName bajo el eventName actual, lo inicializa
          if (!acc[item.eventName][item.topicName]) {
            acc[item.eventName][item.topicName] = { totalMissedUsers: 0, totalAttendedUsers: 0 };
          }
          // Suma los valores para totalMissedUsers y totalAttendedUsers
          acc[item.eventName][item.topicName].totalMissedUsers += item.totalMissedUsers;
          acc[item.eventName][item.topicName].totalAttendedUsers += item.totalAttendedUsers;
          // Suma los valores para countAppointmentXTopic
          if (!acc[item.eventName][item.topicName].countAppointmentXTopic) {
            acc[item.eventName][item.topicName].countAppointmentXTopic = 0;
          }
          acc[item.eventName][item.topicName].countAppointmentXTopic += item.countAppointmentXTopic;
          acc[item.eventName][item.topicName].virtualAppointments = item.virtualAppointments;
          acc[item.eventName][item.topicName].inPersonAppointments = item.inPersonAppointments;
          acc[item.eventName][item.topicName].averageTime = item.averageTime;
          return acc;
        }, {});
        this.data = groupedData;
        this.getReportAppointment();
        this.loanding=false;
      },
      (error: any) => {
        console.log(error);
        this.messageService.add({severity:'error', summary:'Error', detail: error.error});
        this.loanding=false;
      }
      );
  }

  getChartData(event: any) {
    this.chartsData={};
    this.totalscheduled=[];
    this.totalesAppointment=[];
    this.totalVirutalAppointments=[];
    this.totalInPersonAppointments=[];
    this.averageTime=[];
    let groupedData = this.data;
    console.log('data agrupada: ',groupedData);
    let eventName = event.name;
    eventName = Object.keys(groupedData).find(key => key === eventName)||'';
    if(eventName!=''){
      const topics = Object.keys(groupedData[eventName]);
      let total: number = 0;
      let totalAppointments = 0;
      let missed:Array<number>=[];
      let attended:Array<number>=[];
      let totalInPersonAppointments=0;
      let totalVirtualAppointments=0;
      let[totalMissedUsers, totalAttendedUsers] = topics.map(topic => {
      let totalMissedUsers = groupedData[eventName][topic].totalMissedUsers;
      totalMissedUsers = totalMissedUsers > 0 ? totalMissedUsers - groupedData[eventName][topic].countAppointmentXTopic : totalMissedUsers;
      missed.push(totalMissedUsers);
      let totalAttendedUsers = groupedData[eventName][topic].totalAttendedUsers;
      attended.push(totalAttendedUsers);
      totalAppointments = totalAppointments + groupedData[eventName][topic].countAppointmentXTopic;
      total = total + totalAttendedUsers + totalMissedUsers;
      totalInPersonAppointments = totalInPersonAppointments + groupedData[eventName][topic].inPersonAppointments;
      totalVirtualAppointments = totalVirtualAppointments + groupedData[eventName][topic].virtualAppointments;
      this.averageTime.push(groupedData[eventName][topic].averageTime);
      return [totalMissedUsers, totalAttendedUsers];
    });
      this.totalInPersonAppointments.push(totalInPersonAppointments);
      this.totalVirutalAppointments.push(totalVirtualAppointments);
      this.totalscheduled.push(total);
      this.totalesAppointment.push(totalAppointments);
      this.getChart(eventName,topics, missed, attended);
    }
    else {
      this.messageService.add({severity:'error', summary:'Error', detail: 'No se encontraron datos para el evento '+eventName});
    }

  }

  getChart(eventName: string,topics: string[], totalMissedUsersData: number[], totalAttendedUsersData: number[]) {
    this.chartsData[eventName] = {
      labels: topics,
      datasets: [
        {
          type: 'bar',
          label: 'No asistieron ',
          backgroundColor: '#b32b23',
          data: totalMissedUsersData
        },
        {
          type: 'bar',
          label: 'Asistieron',
          backgroundColor: "#BF8924",
          data: totalAttendedUsersData
        }
      ]
    };
  }

  getyears(){
    let fecha = new Date();
    let anio = fecha.getFullYear();
    this.years=[];
    for (let i = 2024; i <= anio; i++) {
      this.years.push({name: i, value: i});
    }
  }


  getEventNames(): string[] {
    return Object.keys(this.chartsData);
  }

  initChart() {
  this.options = {
    maintainAspectRatio: false,
    aspectRatio: 0.8,
    plugins: {
        tooltip: {
            mode: 'index',
            intersect: false
        },
        legend: {
            labels: {
                color: this.textColor
            }
        }
    },
    scales: {
        x: {
            stacked: true,
            ticks: {
                color: this.textColorSecondary
            },
            grid: {
                color: this.surfaceBorder,
                drawBorder: false
            }
        },
        y: {
            stacked: true,
            ticks: {
                color: this.textColorSecondary
            },
            grid: {
                color: this.surfaceBorder,
                drawBorder: false
            }
        }
    }
  };
  const documentStyle = getComputedStyle(document.documentElement);
  this.textColor = documentStyle.getPropertyValue('--text-color');
  this.textColorSecondary = documentStyle.getPropertyValue('--text-color-secondary');
  this.surfaceBorder = documentStyle.getPropertyValue('--surface-border');
  }


}

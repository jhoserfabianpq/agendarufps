import { Component, ElementRef, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import { AppointmentService } from '../../services/APIAppointment/appointment.service';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-assistants',
  templateUrl: './assistants.component.html',
  styleUrl: './assistants.component.css',
  providers: [MessageService]
})
export class AssistantsComponent {

  events!: any[];
  eventsUpcoming!: any[] ;
  user_appointment:any[]=[];
  loadingApoiments=false;
  loadingUpcomingApoiments=false;
  loading: boolean = false;
  user: any;
  myEmail!: string;
  loadingTable!: any[] ;
  stateOptions: any[] = [{label: 'Si', value:false}, {label: 'No', value: true}];
  value!: string ;

  @ViewChild('filter') filter!: ElementRef;
  @ViewChild('dt1') table: Table | undefined;

  constructor(
    private appointmentAPI: AppointmentService,
    private messageService: MessageService
  ) { }


  ngOnInit(){
    this.getAllCreatorAppointments();
    this.user=JSON.parse(localStorage.getItem('user') || '{}');
    this.loadingTable = Array.from({ length: 5 }).map((_, i) => `Item #${i}`);
    this.getUpcomingAppointments();
  }

  getUpcomingAppointments() {
    
    this.loadingUpcomingApoiments = true;
    this.appointmentAPI.getUpcomingAppointments().subscribe(
      (res: any) => {
        this.eventsUpcoming = res;
        this.loadingUpcomingApoiments = false;
        
      },
      (err: any) => {
        
        this.loadingUpcomingApoiments = false;
      }
    )
  }

  onGlobalFilter(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.table?.filterGlobal(value, 'contains');
  }

  clear(table: Table) {
    table.clear();
  }

  exportCSVCustom(): void {
  const headers = 'Nombre Evento,Ver Evento,Tipo Evento,Lugar Evento,Tema Agendado,Fecha Evento,Hora Evento,Duracion Evento,Asistentes\n';
  const rows = this.events.map(event => {
    const assistantList = event.attendees
      .map((assistant: { attended: any; user: { first_name: any; last_name: any; email: any; }; }) => {
        // Aquí decides qué información del asistente incluir
        const attendedStatus = assistant.attended ? 'Asistió' : 'No Asistió';
        return assistant.user.first_name ? `${assistant.user.first_name} ${assistant.user.last_name} (${assistant.user.email}) - ${attendedStatus}` : `Usuario No Registrado - ${attendedStatus}`;
      })
      .join('; '); // Separa cada asistente con '; '

    return [
      `"${event.appointment.event_id.name}"`,
      `"${event.appointment.link_google_calendar}"`,
      `"${event.appointment.event_id.typeMeeting}"`,
      `"${event.appointment.event_id.location}"`,
      `"${event.appointment.topic.name}"`,
      `"${new Date(event.appointment.appointment_start).toLocaleDateString()}"`,
      `"${new Date(event.appointment.appointment_start).toLocaleTimeString()}"`,
      `"${event.appointment.event_id.duration} Min"`,
      `"${assistantList}"` // Añade la lista de asistentes con el estado de asistencia
    ].join(',');
  });

  const csvContent = headers + rows.join('\n');
  this.downloadCSV(csvContent);
  }


  downloadCSV(csvContent: string): void {
  const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
  const url = window.URL.createObjectURL(blob);
  const link = document.createElement('a');
  link.setAttribute('href', url);
  link.setAttribute('download', 'events.csv');
  document.body.appendChild(link); // Necesario para Firefox
  link.click();
  document.body.removeChild(link);
  }


  getAllCreatorAppointments() {
  this.loadingApoiments = true;
  this.myEmail = JSON.parse(localStorage.getItem('user') || '{}').email;
  this.appointmentAPI.getAllCreatorAppointments().subscribe(async (res: any) => {
    this.events = res;
    this.loadingApoiments = false;
  },
  (err: any) => {
    
    this.loadingApoiments = false;
    this.messageService.add({severity:'error', summary:'Error', detail: err.error});
  });
  }

  markAttended(idApointment:number, idUser:number, attended: boolean){
  

  this.appointmentAPI.markAttended(idApointment, idUser,attended).subscribe((res:any) => {
    
    this.messageService.add({severity:'success', summary:'Success', detail:'El usuario se actualizo'});
  },
  (err: any) => {
    
    this.messageService.add({severity:'error', summary:'Error', detail: err.error});
  });
  }

}

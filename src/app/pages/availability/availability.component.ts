import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray,AbstractControl  } from '@angular/forms';
import { AvailabilityService } from '../../services/APIAvailability/availability.service';
import { MessageService } from 'primeng/api';
import { Availability, WeekDays } from './availability.model';
import { UserService } from '../../services/APIUser/user.service';
import { state } from '@angular/animations';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css'],
  providers: [MessageService]
})
export class AvailabilityComponent implements OnInit {


  display: boolean = false;
  availabilities!: Availability[];
  form!: FormGroup;
  public tempForm!: FormGroup;
  loadingCreateAvailability=false;



  constructor(
    private availabilityAPI: AvailabilityService,
    private messageService: MessageService,
    private formBuilder: FormBuilder,
    private userApi: UserService
  ) {
  }


  ngOnInit(): void {
    this.getTopics();
    this.initForm();

  }

  onDeleteAvailability(id:number){
    this.availabilities = this.availabilities.filter((availability: any) => availability.id_availability !== id);
    this.messageService.add({ key:'cd', severity: 'success', summary: 'Disponibilidad Eliminada', detail: 'La disponibilidad se eliminó de la base de datos'});
  }

  availabilityUpdate(event:any){
    
    this.ngOnInit();

  }


  initForm(): void {
    const days = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
    const weekDays = days.map(day => {
      return this.formBuilder.group({
        name: day,
        state: [false],
        intervals: this.formBuilder.array([])
      });
    });

    this.form = this.formBuilder.group({
      availabilityName: ['', Validators.required],
      weekDays: this.formBuilder.array(weekDays)
    });
  }

  get weekDays (){
    return this.form.get('weekDays') as FormArray
  }

  castToFormGroup(control: AbstractControl): FormGroup{
  return control as FormGroup;
  }

  addInterval(){
    const intervals = this.form.get("intervals") as FormArray;

    if(this.tempForm.valid){
      intervals.push(this.formBuilder.group({
        ...this.tempForm.value
      }))

      this.tempForm.setValue({
        date_start: null,
        date_end: null
      })
    }
  }

  deleteInterval(index: number){
    const intervals = this.form.get("intervals") as FormArray;
    intervals.removeAt(index)
  }

  registerForm() {
    this.loadingCreateAvailability=true;
    if (this.ValidateForm()) {
      
      const availability:Availability={
        name: '',
        createdBy: {
          email:''
        },
        id_availability: 0,
        weekDays: []
      };

      const weekDays:WeekDays={
        id_week: 0,
        name: '',
        state: false,
        intervals: []
      };

      availability.name = this.form.value.availabilityName;
      
      availability.weekDays = this.form.value.weekDays.filter((day: any) => day.state === true);

    this.getUserEmail().then((
      email: string) => {
        availability.createdBy.email = email;
        
      }
    );
    availability.weekDays.map((day: any) => {
      
      day.state = 1;
      day.intervals = day.intervals.map((interval: any) => {
        const startTime = new Date(interval.date_start);
        const endTime = new Date(interval.date_end);
        const formattedStartTime = startTime.toTimeString().split(' ')[0];
        const formattedEndTime = endTime.toTimeString().split(' ')[0];
        return {
          date_start: formattedStartTime,
          date_end: formattedEndTime
        };
      });
      return day;
    });
      this.availabilityAPI.postTopic(availability).subscribe(
        (data: any) => {
          
          this.display = false;
          this.availabilities.push(availability);
          this.messageService.add({ key:'cd', severity: 'success', summary: 'Disponibiliad Registrada', detail: 'La disponibilidad se registro en la base de datos'});
          this.loadingCreateAvailability=false;
          this.ngOnInit();
          this.form.reset({
            availabilityName: null,
            weekDays: this.form.value.weekDays.map((day: any) => {
              return {
                ...day,
                state: null,
                intervals: day.intervals = []
              };
            })
          });
        },
        (error: any) => {
          this.messageService.add({ key:'cd', severity: 'error', summary: 'Error', detail:error.error});
          this.loadingCreateAvailability=false;
        });
    }
    // Aquí puedes agregar el código para enviar el formulario a la API
  }

  getUserEmail(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.userApi.getUser().subscribe(
        (data: any) => {
          const email = data.email;
          resolve(email);
        },
        (error: any) => {
          reject(error);
        }
      );
    });
  }




  getTopics() {
    this.availabilityAPI.getAvailability().subscribe(
      (data: any) => {
        //
        this.setAvailabilities(data);
      },
      (error: any) => {
        
        this.messageService.add({ key:'cd', severity: 'error', summary: 'Error', detail:error.error});
      });
  }

  setAvailabilities(data: any) {
    this.availabilities = data;
  }

  ValidateForm(): boolean {
    this.form.markAllAsTouched();
    return this.form.valid;
  }



}

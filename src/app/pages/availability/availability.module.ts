import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvaiCardComponent } from './avai-card/avai-card.component';
import { SharedModule } from '../../shared/shared.module';
import { TimeComponent } from './time/time.component';



@NgModule({
  declarations: [
    AvaiCardComponent,
    TimeComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AvaiCardComponent,
    TimeComponent
  ]
})
export class AvailabilityModule { }

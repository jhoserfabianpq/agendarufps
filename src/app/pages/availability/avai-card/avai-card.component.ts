import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Availability, WeekDays } from '../availability.model';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AvailabilityService } from '../../../services/APIAvailability/availability.service';
import { ConfirmationService, MessageService } from 'primeng/api';

@Component({
  selector: 'app-avai-card',
  templateUrl: './avai-card.component.html',
  styleUrl: './avai-card.component.css',
  providers: [MessageService,ConfirmationService]
})
export class AvaiCardComponent {

  display: boolean = false;
  availabilities!: Availability[];
  form!: FormGroup;
  public tempForm!: FormGroup;
  loandingSaveAvailability=false;
  loandingDeleteAvailability=false;

  constructor(
    private formBuilder: FormBuilder,
    private availabilityAPI:AvailabilityService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
  }

  @Input() availability!:Availability;
  @Output() availabilityDeleted: EventEmitter<any> = new EventEmitter<any>();
  @Output() availabilityUpdate: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
    this.initForm();

  }

  ValidateForm(): boolean {
    this.form.markAllAsTouched();
    return this.form.valid;
  }

  updateForm() {
    this.loandingSaveAvailability=true;
    
    if (this.ValidateForm()) {
      
      const availability:Availability={
        name: '',
        createdBy: {
          email:''
        },
        weekDays: [],
        id_availability: 0
      };
      availability.name = this.form.value.availabilityName;
      availability.createdBy = this.form.value.createdBy;
      availability.id_availability = this.form.value.id_availability;
      availability.weekDays = this.form.value.weekDays
        .filter((weekday: any) => weekday.state)
        .map((weekday: any) => {
          return {
            ...weekday,
            state: weekday.state ? 1 : 0,
            intervals: weekday.intervals.map((interval: any) => {
              const startTime = new Date(interval.date_start);
              const endTime = new Date(interval.date_end);
              const formattedStartTime = startTime.toTimeString().split(' ')[0];
              const formattedEndTime = endTime.toTimeString().split(' ')[0];
              return {
                date_start: formattedStartTime,
                date_end: formattedEndTime
              };
            })
          };
        });
      this.availabilityAPI.putAvailability(availability).subscribe(
        (data: any) => {
          
          this.messageService.add({key:'ua',severity:'success', summary:'Success', detail:'Availability updated'});
          this.display = false;
          this.loandingSaveAvailability=false;
          this.availabilityUpdate.emit(availability);
          },
        (error: any) => {
          
          this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:error.error});
          this.loandingSaveAvailability=false;
        });
    }
  }

  initForm(): void {
    const days = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
    const weekDays = days.map(day => {
      return this.formBuilder.group({
        name: day,
        state: [false],
        intervals: this.formBuilder.array([])
      });
    });

    this.form = this.formBuilder.group({
      availabilityName: ['', Validators.required],
      weekDays: this.formBuilder.array(weekDays),
      createdBy: [''],
      id_availability :['']

    });
  }

  get weekDays (){
    return this.form.get('weekDays') as FormArray
  }

  addInterval(){
    const intervals = this.form.get("intervals") as FormArray;

    if(this.tempForm.valid){
      intervals.push(this.formBuilder.group({
        ...this.tempForm.value
      }))

      this.tempForm.setValue({
        date_start: null,
        date_end: null
      })
    }
  }

  deleteInterval(index: number){
    const intervals = this.form.get("intervals") as FormArray;
    intervals.removeAt(index)
  }

  castToFormGroup(control: AbstractControl): FormGroup{
    return control as FormGroup;
    }

    confirmDelete(event: Event,id:number) {
      this.confirmationService.confirm({
          target: event.target as EventTarget,
          message: '¿Quiere eliminar la disponibilidad?',
          icon: 'pi pi-info-circle',
          acceptButtonStyleClass: 'p-button-danger p-button-sm',
          accept: () => {
            this.deleteAvailability(id);
          }
      });
    }

  deleteAvailability(id: number) {
    this.loandingDeleteAvailability=true;
    
    this.availabilityAPI.deleteAvailability(id).subscribe(
      (data: any) => {
        
        this.availabilityDeleted.emit(id);
        this.loandingDeleteAvailability=false;
      },
      (error: any) => {
        
        
        this.messageService.add({key:'ua',severity:'error', summary:'Error', detail:error.error});
        this.loandingDeleteAvailability=false;
      });
  }

  editAvailability(availability: any) {
    this.display=true;
    
    // Establece el nombre de la disponibilidad.
    this.form.patchValue({
      availabilityName: availability.name,
      createdBy: availability.createdBy,
      id_availability: availability.id_availability
    });

    // Obtiene el array de FormArray de weekDays en el formulario.
    const weekDaysFormArray = this.form.get('weekDays') as FormArray;

    availability.weekDays.forEach((availabilityDay: any) => {
      // Utiliza find para buscar el FormGroup correcto, asegurando primero que sea tratado como FormGroup.
      const dayFormGroup = weekDaysFormArray.controls.find((control) => {
        // Asegura que el control sea tratado como FormGroup antes de acceder a sus propiedades.
        const formGroup = control as FormGroup;
        return formGroup.controls['name'].value === availabilityDay.name;
      }) as FormGroup | undefined; // Asegúrate de manejar el caso en que el FormGroup no se encuentre.

      if (dayFormGroup) {
        // Asegúrate de que el control exista antes de intentar leer su valor.
        const nameControl = dayFormGroup.get('name');
        if (nameControl) {
          // Ahora es seguro usar nameControl.value y otros métodos/propiedades.
          let check = false;
          if(availabilityDay.state){
            check = true;
          }
          dayFormGroup.patchValue({
            state: check,
          });

          const intervalsFormArray = dayFormGroup.get('intervals') as FormArray;
          intervalsFormArray.clear(); // Limpia los intervalos existentes para evitar duplicados.

          availabilityDay.intervals.forEach((interval: any) => {
            

            const dateStart=new Date();
            dateStart.setHours(interval.date_start.split(":")[0], interval.date_start.split(":")[1], interval.date_start.split(":")[2]);
            const dateEnd=new Date();
            dateEnd.setHours(interval.date_end.split(":")[0], interval.date_end.split(":")[1], interval.date_end.split(":")[2]);
            intervalsFormArray.push(this.formBuilder.group({
              id_intervals: interval.id_intervals,
              date_start: dateStart,
              date_end: dateEnd,
            }));
          });
        }
      }
    });
    
  }


}

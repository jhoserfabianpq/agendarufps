import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrl: './time.component.css'
})
export class TimeComponent {
  @Input() dayForm!:FormGroup
  public tempForm!: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    ) {}

  ngOnInit(): void {
    this.tempForm = this.formBuilder.group({
      date_start: [null],
      date_end: [null]
    });
    this.tempForm.get('date_start')?.valueChanges.subscribe(value => {
      this.startHourChage(value);
    });
  }

  addInterval(){
    const intervals = this.dayForm.get("intervals") as FormArray;
    if(this.tempForm.valid){
      intervals.push(this.formBuilder.group({
        ...this.tempForm.value
      }))

      this.tempForm.setValue({
        date_start: null,
        date_end: null
      })
    }
    
  }

  deleteInterval(index: number){
    const intervals = this.dayForm.get("intervals") as FormArray;
    intervals.removeAt(index)
  }

  startHourChage(value:any){
    
    this.tempForm.get('date_end')?.setValue(this.tempForm.get('date_start')?.value);
  }

}

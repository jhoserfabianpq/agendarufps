export interface Availability {
  name: string,
  createdBy: UserInfo,
  id_availability: number,
  weekDays:WeekDays[]
}

export interface Intervals{
  id_intervals: number,
  date_start: string,
  date_end: string
}
export interface WeekDays  {
  id_week: number,
  name: string,
  state: boolean,
  intervals: Intervals[]
}
interface UserInfo{
  email: string,
}

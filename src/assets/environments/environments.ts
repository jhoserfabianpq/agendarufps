import { Availability } from '../../app/pages/availability/availability.model';
export const environment = {
  production: false,
  host: 'https://studentsprojects.cloud.ufps.edu.co/agendarbackend/agendarbackend',
  hostFront: 'https://agendarufps.vercel.app/',
  host2: 'http://localhost:8080/agendarbackend',
  hostFront2: 'http://localhost:4200/agendarUFPS',
  eventBaseEndpoint: '/event/',
  eventBaseEndpointPublic: '/public/event/',
  AvailabilityBaseEndpoint: '/availability/',
  topicBaseEndpoint: '/topic/',
  userBaseEndpoint: '/user',
  appointmentBaseEndpoint: '/appointment/',
  loginGoogleBaseEndpoint: '/auth/url',
  reportBaseEnpoint:'/report-general/',
  reportBaseEnpointAppointment: '/report-appointment/'
};
